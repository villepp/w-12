import VAMK_logo from './VAMK_logo.png';
import './App.css';

function App() {
  return (
    <div className="App">
      <img src={VAMK_logo} alt="VAMK logo" className="logo" />
      <p>
        <span style={{ fontWeight: 'bold', color: 'purple', fontSize: '30px' }}>Ville Pitkänen</span><br /><br />
        <span style={{ color: 'purple' }}>Engineering Student</span><br /><br />
        <span style={{ fontWeight: 'bold' }}>opiskelija</span><br />
        Tietotekniikka<br />
        <span style={{ fontWeight: 'bold' }}>student</span><br />
        Information technology<br /><br />

        <span style={{ color: 'purple' }}>Tekniikan yksikkö</span><br />
        <span style={{ color: 'purple' }}>School of Technology</span>
      </p>
      <p>
        e2300558@edu.vamk.fi<br />
        040 123 456 789<br /><br />
        Wolffintie 30, FI-65200 VAASA, Finland
      </p>
    </div>
  );
}

export default App;
